# toutiao
这是牛客第二次课程的练习
【1】通过spring boot 创建一个maven项目
【2】通过注解的方式实现各种功能
【3】分别在src目录下分为controller、service、model、aspect（为了写log）层，并没有dao层
【4】实现了访问某些路径的信息，可以返回字符串或者一个模板
    实现了通过url进行参数传递
    实现了controller层通过setAttribute方法向模板传递参数
    实现了通过Request对象获取request内容比如cookie，头信息等；通过cookie中°信息并写回到response中
    实现两种重定向，301表示永久重定向，302表示暂时重定向
    通过AOP的方式切面打log，定义了在访问那些类之前和之后log中显示的信息
