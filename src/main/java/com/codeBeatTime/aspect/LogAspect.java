package com.codeBeatTime.aspect;


import org.apache.log4j.spi.LoggerFactory;
import org.aspectj.lang.JoinPoint;
import java.lang.*;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.omg.CORBA.Object;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;

/**
 * Created by Administrator on 2016/6/28.
 */
@Aspect
@Component
public class LogAspect {
    public static final Logger logger = org.slf4j.LoggerFactory.getLogger(LogAspect.class);
    @Before("execution(* com.codeBeatTime.controller.IndexController.*(..))")
    public  void beforeMethod(JoinPoint joinPoint){
        StringBuilder sb = new StringBuilder();
        for(java.lang.Object arg :joinPoint.getArgs()){
                sb.append(arg.toString());
        }
            logger.info("before Method"+sb.toString()+"|");
    }
    @After("execution(* com.codeBeatTime.controller.IndexController.*(..))")
    public  void afterMethod(JoinPoint joinPoint){
            logger.info("after Method");
    }
}
