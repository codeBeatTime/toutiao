package com.codeBeatTime.controller;

import java.util.*;

import com.codeBeatTime.model.User;
import com.codeBeatTime.service.TouTiaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Arrays;


/**
 * Created by Administrator on 2016/6/27.
 */
@Controller
public class IndexController {
    @Autowired
    private TouTiaoService toutiaoService;
    @RequestMapping(path= {"/","/index"})
    @ResponseBody
    public String index(){

        return "Hello World" +" "+ toutiaoService.say();
    }
    @RequestMapping(path = {"/profile/{groupId}/{userId}"})
    @ResponseBody
    public String profile(@PathVariable("groupId") String groupId,@PathVariable("userId") int userId,
                          @RequestParam(value = "type",defaultValue="1") int type,@RequestParam(value = "key",defaultValue = "codeBeatTime") String key
    ){
        return String.format("{%s},{%d},{%d},{%s}",groupId,userId,type,key);
    }
    @RequestMapping(path = {"/vm"})

    public String news(Model model){
        model.addAttribute("value1","vv1");
        List<String> colors= Arrays.asList("RED","BLACK","WHITE");
        model.addAttribute("colors",colors);
        model.addAttribute("user",new User( "Jim"));
        return "news";
    }
    @RequestMapping(value = {"/request"})
    @ResponseBody
    public String request(HttpServletRequest request,
                          HttpServletResponse response,
                          HttpSession session
    ){
        StringBuilder sb = new StringBuilder();
        Enumeration<String> headerNames = request.getHeaderNames();
        while(headerNames.hasMoreElements()){
            String name = headerNames.nextElement();
            sb.append(name+" :  "+ request.getHeader(name)+"<br>");
        }
        for(Cookie cookie:request.getCookies()){
            sb.append(cookie.getName());
            sb.append(" : ");
            sb.append(cookie.getValue());
            sb.append("<br>");
        }
        return sb.toString();
    }
    @RequestMapping(value = {"/response"})
    @ResponseBody
    public String response(@CookieValue(value = "newCoderId",defaultValue = "jj") String newCoderId,
                           @RequestParam(value = "key",defaultValue =   "key") String key,
                           @RequestParam(value = "value",defaultValue = "value" ) String value,
                           HttpServletResponse response
                           ){
            response.addCookie(new Cookie(key,value));
            response.addHeader(key,value);
            return "nowCoderID" + newCoderId;
    }
    @RequestMapping("/redirect/{code}")
    public RedirectView redirect(@PathVariable("code") int code){
        RedirectView red = new RedirectView("/",true);
        if(code==301){
            red.setStatusCode(HttpStatus.MOVED_PERMANENTLY);
        }
        return red;
    }
}
